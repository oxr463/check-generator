#!/bin/sh
# Generate unit tests for c projects via check
# SPDX-License-Identifier: GPL-2.0-or-later
set -eu

oneTimeSetUp() {
  . ./src/generator.sh
}

test_check_deps() {
  result=$(check_deps)

  fail "Not yet implemented"
}

test_check_env_vars() {
  result=$(check_env_vars)

  fail "Not yet implemented"
}

test_get_sources() {
  result=$(get_sources "test/hello")

  assertEquals "test/hello/main.c" "${result}"
}

test_get_functions() {
  fail "Not yet implemented"
}

test_generate_tests() {
  fail "Not yet implemented"
}

test_main() {
  fail "Not yet implemented"
}
