INSTALL = /usr/bin/install
INSTALL_DATA = ${INSTALL} -m 644
SHELLCHECK ?= shellcheck
SHUNIT ?= shunit2

PROGRAM_NAME = check-gen

PREFIX ?= /usr/local
BIN_DIR = ${PREFIX}/bin
SHARE_DIR = ${PREFIX}/share/${PROGRAM_NAME}

BUILD_DIR = .
TARGET_DIR = ${BUILD_DIR}
SRC_DIR = ${BUILD_DIR}/src
TEMPLATE_DIR = ${BUILD_DIR}/template
TEST_DIR = ${BUILD_DIR}/test

SRC = ${SRC_DIR}/main.sh

TEST_SRC = ${TEST_DIR}/main.sh

all: ${PROGRAM_NAME}

${PROGRAM_NAME}:
	@cp ${SRC} ${TARGET_DIR}/${PROGRAM_NAME}
	@sed -i 's|TEMPLATE_DIR=".*"|TEMPLATE_DIR="${SHARE_DIR}/template"|g' ${TARGET_DIR}/${PROGRAM_NAME}
	@chmod +x ${TARGET_DIR}/${PROGRAM_NAME}

check:
	@${SHELLCHECK} ${SRC}

test:
	@${SHUNIT} ${TEST_SRC}

install: ${PROGRAM_NAME}
	@${INSTALL} ${TARGET_DIR}/${PROGRAM_NAME} ${BIN_DIR}
	@mkdir -p ${SHARE_DIR}
	@cp -R ${TEMPLATE_DIR} ${SHARE_DIR}/

clean:
	@rm -rf ${TARGET_DIR}/${PROGRAM_NAME}

.PHONY: all check clean install test
